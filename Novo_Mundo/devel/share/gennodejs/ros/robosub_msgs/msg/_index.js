
"use strict";

let Trax = require('./Trax.js');
let PositionsStamped = require('./PositionsStamped.js');
let control_status = require('./control_status.js');
let visionPos = require('./visionPos.js');
let BatteryDetailed = require('./BatteryDetailed.js');
let rgbArray = require('./rgbArray.js');
let rgb = require('./rgb.js');
let Position = require('./Position.js');
let joystick = require('./joystick.js');
let Float32Stamped = require('./Float32Stamped.js');
let control = require('./control.js');
let ObstaclePosArray = require('./ObstaclePosArray.js');
let Euler = require('./Euler.js');
let visionPosArray = require('./visionPosArray.js');
let thruster = require('./thruster.js');
let PositionArrayStamped = require('./PositionArrayStamped.js');
let DetectionArray = require('./DetectionArray.js');
let ObstaclePos = require('./ObstaclePos.js');
let Detection = require('./Detection.js');
let HydrophoneDeltas = require('./HydrophoneDeltas.js');
let gamepad = require('./gamepad.js');

module.exports = {
  Trax: Trax,
  PositionsStamped: PositionsStamped,
  control_status: control_status,
  visionPos: visionPos,
  BatteryDetailed: BatteryDetailed,
  rgbArray: rgbArray,
  rgb: rgb,
  Position: Position,
  joystick: joystick,
  Float32Stamped: Float32Stamped,
  control: control,
  ObstaclePosArray: ObstaclePosArray,
  Euler: Euler,
  visionPosArray: visionPosArray,
  thruster: thruster,
  PositionArrayStamped: PositionArrayStamped,
  DetectionArray: DetectionArray,
  ObstaclePos: ObstaclePos,
  Detection: Detection,
  HydrophoneDeltas: HydrophoneDeltas,
  gamepad: gamepad,
};
