# CMake generated Testfile for 
# Source directory: /home/henrique/ai_palouse_ws/src
# Build directory: /home/henrique/ai_palouse_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(robosub_msgs)
subdirs(robosub)
subdirs(robosub_simulator)
