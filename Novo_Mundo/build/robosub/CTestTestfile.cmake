# CMake generated Testfile for 
# Source directory: /home/henrique/ai_palouse_ws/src/robosub
# Build directory: /home/henrique/ai_palouse_ws/build/robosub
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src/examples)
subdirs(src/utility)
subdirs(src/movement)
subdirs(src/sensors)
subdirs(src/controllers)
subdirs(src/vision)
subdirs(src/localization)
