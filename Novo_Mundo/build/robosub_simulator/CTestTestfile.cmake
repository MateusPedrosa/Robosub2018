# CMake generated Testfile for 
# Source directory: /home/henrique/ai_palouse_ws/src/robosub_simulator
# Build directory: /home/henrique/ai_palouse_ws/build/robosub_simulator
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(examples)
subdirs(src)
